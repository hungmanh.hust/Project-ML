import csv
import useful_function as uf

path = '/Users/manhhung/Documents/BKHN/ML/workspace/football-prediction/mywork/data/'

list_file = ['E00809.csv', 'E00910.csv', 'E01011.csv', 'E01112.csv', 'E01213.csv', 'E01314.csv', 'E01415.csv',
             'E01516.csv', 'E01617.csv', 'E01718.csv']


#  i la vi tri hien tai, team la doi hien tai
def get_form(i, list, team):
    s = 0
    form = 0
    for line in range(i - 1, -1, -1):

        if list[line][1] == team or list[line][2] == team:
            s += 1
            if (list[line][1] == team and int(list[line][3]) > int(list[line][4])) \
                    or (list[line][2] == team and int(list[line][3]) < int(list[line][4])):
                form += 2
            elif int(list[line][3]) == int(list[line][4]):
                form += 1
        if (s >= 5):
            break

    if s < 5:
        return None

    return form / 10.0


def get_all_forms(path, file, file_extracted):
    f = open(path + "parse_" + file, 'rb')
    reader = csv.reader(f)
    reader = list(reader)
    reader = reader[1:]
    f.close()

    i = 0
    data_extracted_form = [['name1', 'name2', 'form1', 'form2', 'result']]
    for line in reader:
        form1 = get_form(i, reader, line[1])
        form2 = get_form(i, reader, line[2])

        if form2 is not None and form1 is not None:
            result = '0'
            if line[5] == '0':
                result = '-1'
            elif line[5] == '0.5':
                result = '0'
            elif line[5] == '1':
                result = '1'
            data_extracted_form.append([line[1], line[2], str(form1), str(form2), result])
            print(str(i) + ": " + line[1] + "," + line[2] + "," + str(form1) + ", " + str(form2) + ", " + str(line[5]))
        i += 1
    uf.write_file_csv(file_extracted, data_extracted_form)


def extract_form_all_season():
    for file in list_file:
        get_all_forms(path, file, path + "extracted_form_" + file)


def parse_all_data():
    for file in list_file:
        uf.convert_data(path, file)


parse_all_data()
extract_form_all_season()
