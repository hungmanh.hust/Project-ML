import csv
import useful_function as uf

path = '/Users/manhhung/Documents/BKHN/ML/workspace/football-prediction/mywork/data/'
list_file = ['0910.csv', '1011.csv', '1112.csv', '1213.csv', '1314.csv', '1415.csv', '1516.csv', '1617.csv',
             '1718.csv']


def get_diff_score_goals(i, list, home, away):
    winHome = lostHome = winAway = lostAway = score_home = score_away = 0
    for line in range(i - 1, -1, -1):
        if list[line][1] == home:
            winHome += int(list[line][3])
            lostHome += int(list[line][4])
            score_home += float(list[line][5])
        elif list[line][2] == home:
            winHome += int(list[line][4])
            lostHome += int(list[line][3])
            score_home += 1 - float(list[line][5])

        if list[line][1] == away:
            winAway += int(list[line][3])
            lostAway += int(list[line][4])
            score_away += float(list[line][5])
        elif list[line][2] == away:
            winAway += int(list[line][4])
            lostAway += int(list[line][3])
            score_away += 1 - float(list[line][5])

    goal_diff = (winHome - lostHome) - (winAway - lostAway)
    score_diff = (score_home - score_away)
    # print(list[i][1], list[i][2], winHome, lostHome, winAway, lostAway, score_home, score_away, goal_diff, score_diff)

    return [list[i][1], list[i][2], goal_diff, score_diff]


# get all diff goal and score of all season
def get_all_diff():
    all_data = []
    max_goal_dif = max_score_dif = 0
    for file in list_file:
        print(file)
        data = [['name1', 'name2', 'goal_diff', 'score_diff']]
        reader = uf.read_file_parse(path, file)
        i = 0
        for line in reader:
            data.append(get_diff_score_goals(i, reader, line[1], line[2]))
            i += 1
        max_diff = get_max_diff_goal_score(data)
        # print(max_diff)

        j = -1
        for line in data:
            # print(line)
            j += 1
            if j == 0:
                continue
            line[2] = 0.5 + line[2] / (2.0 * max_diff[0])
            line[3] = 0.5 + line[3] / (2.0 * max_diff[1])
            # print(goal, score)

        uf.write_file_csv(path + "extracted_goal_score_diff_" + file, data)


# get max diff goal and score
def get_max_diff_goal_score(data):
    max_diff_goal = max_diff_score = 0
    i = -1
    for line in data:
        i += 1
        if i == 0:
            continue
        if max_diff_goal < float(line[2]):
            max_diff_goal = float(line[2])
        if max_diff_score < float(line[3]):
            max_diff_score = float(line[3])
    return max_diff_goal, max_diff_score


get_all_diff()
