import csv
import useful_function as uf

path = '/Users/manhhung/Documents/BKHN/ML/workspace/football-prediction/mywork/data/'
list_file = ['0809.csv', '0910.csv', '1011.csv', '1112.csv', '1213.csv', '1314.csv', '1415.csv', '1516.csv', '1617.csv',
             '1718.csv']
list_file = list(reversed(list_file))


def get_history(i, list, home, away, current_index_file):
    s = 0
    h = 0
    for line in range(i - 1, -1, -1):
        if (list[line][1] == home and list[line][2] == away) \
                or (list[line][2] == home and list[line][1] == away):
            s += 1
            if (list[line][1] == home and int(list[line][3]) > int(list[line][4])) \
                    or (list[line][2] == home and int(list[line][3]) < int(list[line][4])):
                h += 2
            elif int(list[line][3]) == int(list[line][4]):
                h += 1
        if (s >= 2):
            break

    return get_last_season_history(current_index_file, home, away, s, h)


def get_all_history():
    for current_index_file in range(0, len(list_file) - 1, 1):
        list = uf.read_file_parse(path, list_file[current_index_file])
        i = -1
        data_extracted_history = [['name1', 'name2', 'home', 'away', 'history', 'result']]
        for line in list:
            h = get_history(i, list, line[1], line[2], current_index_file)
            i += 1
            # if i == 0
            if h is not None:
                name_home = line[1]
                name_away = line[2]
                result = '0'
                if line[5] == '0':
                    result = '-1'
                elif line[5] == '0.5':
                    result = '0'
                elif line[5] == '1':
                    result = '1'
                print(str(i) + ": " + line[1] + ", " + line[2] + ", " + str(h) + ", " + result)

                history = str(h)
                data_extracted_history.append([name_home, name_away, '1', '0', history, result])

        uf.write_file_csv(path + "extracted_history_" + list_file[current_index_file], data_extracted_history)


# merge last season, start current current_file
def merge_file(current_file):
    data = []
    for i in range(len(list_file) - 1, current_file, -1):
        data += uf.read_file_parse(path, list_file[i])
    return data


# get history in last season
def get_last_season_history(current_file, home, away, s, h):
    list = merge_file(current_file)

    # print(len(list))
    for line in range(len(list) - 1, -1, -1):
        if (list[line][1] == home and list[line][2] == away) \
                or (list[line][2] == home and list[line][1] == away):
            s += 1
            if (list[line][1] == home and int(list[line][3]) > int(list[line][4])) \
                    or (list[line][2] == home and int(list[line][3]) < int(list[line][4])):
                h += 2
            elif int(list[line][3]) == int(list[line][4]):
                h += 1
        if (s >= 2):
            break

    if s < 2:
        return None

    return h / 4.0


get_all_history()
